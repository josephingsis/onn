pp <?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'responsible-list',
            'responsible-create',
            'responsible-edit',
            'responsible-delete',
            'denomination-list',
            'denomination-create',
            'denomination-edit',
            'denomination-delete',
            'organization-list',
            'organization-create',
            'organization-edit',
            'organization-delete',
            'period-list',
            'period-create',
            'period-edit',
            'period-delete',
            'beneficiary-list',
            'beneficiary-create',
            'beneficiary-edit',
            'beneficiary-delete',
            'annexed-list',
            'annexed-create',
            'annexed-edit',
            'annexed-delete',
            'box-list',
            'box-create',
            'box-edit',
            'box-delete',

        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
        /* Insertamos los datos del usuario */
        $role = Role::create(['name' => 'Administrador']);
        $permissions = Permission::pluck('id', 'id')->all();
        $role->syncPermissions($permissions);
        $users = array(
            ['name' => 'Catherine', 'email' => 'catherinepurificacion@gmail.com', 'password' => bcrypt('catherine'), 'email_verified_at' => '2020-06-10 11:50:06'],
        );
        foreach ($users as $user) {
            $user = User::create($user);
            $user->assignRole([$role->id]);
        }
    }
}
