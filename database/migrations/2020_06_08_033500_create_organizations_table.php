<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->unique();
            $table->string('direccion')->nullable();
            $table->string('ubigeo')->nullable();
            $table->integer('estado')->default(1);
            $table->bigInteger('responsible_id')->unsigned();
            $table->foreign('responsible_id')->references('id')->on('responsibles');
            $table->bigInteger('denomination_id')->unsigned();
            $table->foreign('denomination_id')->references('id')->on('denominations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
