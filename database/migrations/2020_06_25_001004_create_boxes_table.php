<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contacto');
            $table->integer('cajas');
            $table->integer('emr');
            $table->integer('lga');
            $table->integer('nt');
            $table->integer('gm');
            $table->integer('l8');
            $table->integer('cm');
            $table->integer('p');
            $table->bigInteger('responsible_id')->unsigned();
            $table->foreign('responsible_id')->references('id')->on('responsibles');
            $table->bigInteger('organization_id')->unsigned();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->bigInteger('annexed_id')->unsigned();
            $table->foreign('annexed_id')->references('id')->on('annexeds')->nullable();
            $table->bigInteger('period_id')->unsigned();
            $table->foreign('period_id')->references('id')->on('periods');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
    }
}
