<!DOCTYPE html>
<html lang="es">
<head>
    <title>ONN - Huaraz </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="ONN, OCC">
    <meta name="author" content="#">
    <link rel="icon" type="image/svg+xml" href="{{asset('images/occ-logo.svg')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/bootstrap/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('icon/feather/css/feather.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('icon/font-awesome/css/font-awesome.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">

    <link rel="stylesheet" type="text/css"
          href="{{asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

    <!-- Bootstrap Select Css -->
    <link href="{{asset('bower_components/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    @yield('css')
</head>
<body>

<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>

<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">
                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="feather icon-menu"></i>
                    </a>
                    <a href="{{url('/')}}">
                       <img src="{{asset('images/occ-logo.svg')}}" width="40px" alt=""> ONN - HUARAZ
                    </a>
                    <a class="mobile-options">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>
                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!" onclick="if (!window.__cfRLUnblockHandlers) return false; javascript:toggleFullScreen()" data-cf-modified-="">
                                <i class="feather icon-maximize full-screen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">

                        <li class="user-profile header-notification">
                            <div class="dropdown-primary dropdown">
                                <div class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{asset('images/user.png')}}" class="img-radius" alt="User-Profile-Image">
                                    <span>{{ Auth::user()->name}}</span>
                                    <i class="feather icon-chevron-down"></i>
                                </div>
                                <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="feather icon-log-out"></i> Cerrar Sesión
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </li>

                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar">
                    <div class="pcoded-inner-navbar main-menu">
                        <div class="pcoded-navigatio-lavel">Registros</div>
                        <ul class="pcoded-item pcoded-left-item">
                            <li class="">
                                <a href="{{ route('home') }}">
                                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                    <span class="pcoded-mtext">Inicio</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('periods.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-calendar"></i></span>
                                    <span class="pcoded-mtext">Periodo</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('responsibles.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                    <span class="pcoded-mtext">Responsables</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('denominations.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-award"></i></span>
                                    <span class="pcoded-mtext">Denominación</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('organizations.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-map-pin"></i></span>
                                    <span class="pcoded-mtext">Iglesia/Organización</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('annexeds.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-share-2"></i></span>
                                    <span class="pcoded-mtext">Anexos</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('beneficiaries.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-user-check"></i></span>
                                    <span class="pcoded-mtext">Beneficiarios</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('boxes.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-package"></i></span>
                                    <span class="pcoded-mtext">Cajas ONN</span>
                                </a>
                            </li>
                        </ul>
                        <div class="pcoded-navigatio-lavel">Administración</div>
                        <ul class="pcoded-item pcoded-left-item">
                            <li class="">
                                <a href="{{ route('roles.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-lock"></i></span>
                                    <span class="pcoded-mtext">Roles</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('users.index') }}">
                                    <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                                    <span class="pcoded-mtext">Usuarios</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                @yield('content')
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{asset('bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/bootstrap/js/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

<script type="text/javascript" src="{{asset('bower_components/modernizr/js/modernizr.js')}}"></script>

<script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/SmoothScroll.js')}}"></script>
<script src="{{asset('js/pcoded.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/vartical-layout.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/script.min.js')}}"></script>

<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('pages/data-table/js/jszip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('pages/data-table/js/pdfmake.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('pages/data-table/js/vfs_fonts.js')}}" type="text/javascript"></script>
<script src="{{asset('bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"
        type="text/javascript"></script>

<script src="{{asset('bower_components/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('bower_components/sweetalert2/sweet-alert.init.js')}}"></script>

<!-- Select Plugin Js -->
<script src="{{asset('bower_components/bootstrap-select/bootstrap-select.min.js')}}"></script>

@yield('js')
</body>
</html>
