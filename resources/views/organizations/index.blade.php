@extends('layouts.app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Iglesias-Organizaciones</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Iglesias-Organizaciones</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="pull-right">
                                            <a class="btn btn-success btn-round btn-sm"
                                               href="{{ route('organizations.create') }}"><i
                                                        class="demo-pli-add"></i> Nuevo</a>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="table"
                                                   class="table table-striped table-bordered nowrap">
                                                <thead>
                                                <tr>
                                                    <th>N°</th>
                                                    <th>Denominación</th>
                                                    <th>Nombre de Iglesia</th>
                                                    <th>Responsable</th>
                                                    <th>Dirección</th>
                                                    <th>Ubigeo</th>
                                                    <th width="280px">Acciones</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        let tabla = $('#table').DataTable(
            {
                "processing": true,
                "serverSide": true,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "No results matched": "No se encontraron resultados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                },
                "ajax": {
                    "url": "{{ route('organizations.all') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}"
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "denominacion"},
                    {"data": "nombre"},
                    {"data": "responsable"},
                    {"data": "direccion"},
                    {"data": "ubigeo"},
                    {"data": "options"}],
                "columnDefs": [
                    {"className": "text-center", "targets": [0, 1, 2]},
                    {"bSortable": false, "aTargets": [3,4,5,6]},
                ],

            }
        );
        $('#table tbody').on('click', '.delete-confirm', function () {
            let id = $(this).attr('data-id');
            let url = '{!! url('organizations') !!}/' + id;
            Swal.fire({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, elimínalo!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            '_method': 'DELETE',
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (response) {
                            if (response.success) {
                                Swal.fire(
                                    '¡Correcto!',
                                    'El registro ha sido eliminado.',
                                    'success'
                                );
                                tabla.ajax.reload();
                            } else {
                                Swal.fire(
                                    '¡Incorrecto!',
                                    'Hubo un error al eliminar.',
                                    'error'
                                );
                            }
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            })
        });

    </script>
@endsection
