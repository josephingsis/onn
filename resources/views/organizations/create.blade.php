@extends('layouts.app')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">

            <div class="main-body">
                <div class="page-wrapper">

                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Agregar Iglesia-Organización</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('organizations.index') }}">Iglesias-Organizaciones</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Agregar organización</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        {!! Form::open(array('route' => 'organizations.store','method'=>'POST')) !!}
                                            @csrf
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Denominación:</strong>
                                                        {{ Form::select('denomination_id', $denominations->pluck('nombre', 'id'), null, array('id' => 'denomination_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Nombre:</strong>
                                                        {!! Form::text('nombre', null, array('id'=>'nombre','placeholder' => 'Nombre','class' => 'form-control','required')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Responsable:</strong>
                                                        {{ Form::select('responsible_id', $responsibles->pluck('nombre', 'id'), null, array('id' => 'responsible_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Dirección:</strong>
                                                        {!! Form::text('direccion', null, array('id'=>'direccion','placeholder' => 'Dirección','class' => 'form-control','required')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Departamento:</strong>
                                                        {{ Form::select('department_id', $departments->pluck('name', 'id'), null, array('id' => 'department_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Provincia:</strong>
                                                        {{ Form::select('province_id',[] ,null, array('id' => 'province_id','class' => 'selectpicker form-control','title'=>'Seleccione...','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Distrito:</strong>
                                                        {{ Form::select('ubigeo',[], null, array('id' => 'ubigeo','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary m-b-0">Guardar
                                                </button>
                                                <a href="{{route('organizations.index')}}" type="button"
                                                   class="btn btn-warning" type="reset">Cancelar</a>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            cargar_provincias();
            cargar_distritos();

            function cargar_provincias() {
                $('#department_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('department.provinces') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let provincias = '';
                            $.each(data, function (i, value) {
                                provincias = provincias + '<option value="' + value['id'] + '">' + value['name'] + '</option>';
                            });
                            $('#province_id').html(provincias)
                                .selectpicker('refresh');
                            $('#ubigeo').html('').selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }

            function cargar_distritos() {
                $('#province_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('province.districts') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let distritos = '';
                            $.each(data, function (i, value) {
                                distritos = distritos + '<option value="' + value['id'] + '">' + value['name'] + '</option>';
                            });
                            $('#ubigeo').html(distritos)
                                .selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }
        });

    </script>
@endsection


