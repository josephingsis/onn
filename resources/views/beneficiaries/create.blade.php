@extends('layouts.app')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">

            <div class="main-body">
                <div class="page-wrapper">

                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Agregar beneficiario</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('beneficiaries.index') }}">Beneficiarios</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Agregar beneficiario</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        <form class="form-horizontal" id="beneficiario" method="POST"
                                              action="{{route('beneficiaries.store')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Iglesia/Organización:</strong>
                                                        {{ Form::select('organization_id', $organizations->pluck('nombre', 'id'), null, array('id' => 'organization_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Anexo/Lugar:</strong>
                                                        {{ Form::select('annexed_id', [], null, array('id' => 'annexed_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Periodo:</strong>
                                                        {{ Form::select('period_id', $periods->pluck('descripcion', 'id'), null, array('id' => 'period_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <strong>DNI:</strong>
                                                    <div class="input-group">
                                                        {!! Form::text('dni', null, array('id'=>'dni','placeholder' => 'DNI','class' => 'form-control','required')) !!}
                                                        <div class="input-group-btn">
                                                            <button id="reniec" class="btn btn-info" type="button">
                                                                <i class="feather icon-search"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Nombres:</strong>
                                                        {!! Form::text('nombres', null, array('id'=>'nombres','placeholder' => 'Nombres','class' => 'form-control','required')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Apellidos:</strong>
                                                        {!! Form::text('apellidos', null, array('id'=>'apellidos','placeholder' => 'Apellidos','class' => 'form-control','required')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Género:</strong>
                                                        <div class="col-sm-10">
                                                            <select name="genero" class="form-control show-tick">
                                                                <option>Seleccione...</option>
                                                                <option>F</option>
                                                                <option>M</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Fecha de Nacimiento:</strong>
                                                        {!! Form::date('fecha_nac', null, array('id'=>'fecha_nac','placeholder' => 'Fecha de nacimiento','class' => 'form-control','required')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Dirección:</strong>
                                                        {!! Form::text('direccion', null, array('id'=>'direccion','placeholder' => 'Dirección','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Departamento:</strong>
                                                        {{ Form::select('department_id', $departments->pluck('name', 'id'), null, array('id' => 'department_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Provincia:</strong>
                                                        {{ Form::select('province_id',[] ,null, array('id' => 'province_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Distrito:</strong>
                                                        {{ Form::select('ubigeo',[], null, array('id' => 'ubigeo','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" class="btn btn-primary m-b-0">Guardar
                                                    </button>
                                                    <a href="{{route('beneficiaries.index')}}" type="button"
                                                       class="btn btn-warning" type="reset">Cancelar</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            cargar_provincias();
            cargar_distritos();
            cargar_anexos();
            cargar_datos();

            function cargar_provincias() {
                $('#department_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('department.provinces') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let provincias = '';
                            $.each(data, function (i, value) {
                                provincias = provincias + '<option value="' + value['id'] + '">' + value['name'] + '</option>';
                            });
                            $('#province_id').html(provincias)
                                .selectpicker('refresh');
                            $('#ubigeo').html('').selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }

            function cargar_distritos() {
                $('#province_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('province.districts') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let distritos = '';
                            $.each(data, function (i, value) {
                                distritos = distritos + '<option value="' + value['id'] + '">' + value['name'] + '</option>';
                            });
                            $('#ubigeo').html(distritos)
                                .selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }

            function cargar_anexos() {
                $('#organization_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('organization.annexeds') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let anexos = '';
                            $.each(data, function (i, value) {
                                anexos = anexos + '<option value="' + value['id'] + '">' + value['nombre'] + '</option>';
                            });
                            $('#annexed_id').html(anexos)
                                .selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }

            function cargar_datos() {
                $('#reniec').click(function () {
                    let dni = $('#dni').val();
                    if (dni.length === 8) {
                        $.ajaxSetup({
                            async: false
                        });
                        $.ajax({
                            "crossDomain": true,
                            "url": "http://servicio.fitcoders.com/v1/all",
                            "method": "POST",
                            "headers": {
                                "Content-Type": "application/json",
                            },
                            "processData": false,
                            "data": '{"id": "' + dni + '","key": "5b54cd7434a49a0fd4cd5ed2","service": "DNI"}',
                            beforeSend: function () {
                                $('#reniec').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
                            },
                            success: function (response) {
                            },
                            complete: function (data) {
                                $('#reniec').html('<i class="feather icon-search"></i>');
                            }
                        }).done(function (response) {
                            if (response['success']) {
                                console.log(response);
                                $.each(response, function (index, value) {
                                    $("#nombres").val(value['nombre']);
                                    $("#apellidos").val(value['paterno'] + ' ' + value['materno']);
                                    let ubigeo = '020102';
                                    let departamento = ubigeo.substring(0,2);
                                    let provincia = ubigeo.substring(0,4);
                                    let distrito = ubigeo.substring(0,6);
                                    console.log(departamento,provincia,distrito);
                                    $('#department_id').selectpicker('val', departamento);
                                    $('#province_id').selectpicker('val', provincia);
                                    $('#ubigeo').selectpicker('val', distrito);
                                });
                                /*new PNotify({
                                    title: 'Información',
                                    text: 'Datos cargados correctamente desde API',
                                    hide: true,
                                    type: 'info',
                                    styling: 'bootstrap3',
                                    addclass: "stack-bottomleft",
                                });*/
                            } else {
                                alert(response['message']);
                                /*  new PNotify({
                                      title: 'Información',
                                      text: response['message'],
                                      type: 'info',
                                      hide: true,
                                      styling: 'bootstrap3',
                                      addclass: "stack-bottomleft",
                                  });*/
                            }
                        });

                    } else {
                        alert('Número de documento incorrecto')
                        /*new PNotify({
                            title: 'Información',
                            text: 'Número de documento incorrecto',
                            hide: true,
                            type: 'error',
                            styling: 'bootstrap3',
                            addclass: "stack-bottomright",
                        });*/
                        $(this).focus();
                    }
                    //console.log($(this).val());
                });
            }
        });

    </script>
@endsection


