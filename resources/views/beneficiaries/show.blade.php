@extends('layouts.app')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Detalle beneficiario</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('beneficiaries.index') }}">Beneficiarios</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Detalle beneficiario</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Periodo:</b></strong>
                                                {{ $beneficiary->periodo }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Iglesia/Organización: </b></strong>
                                                {{ $beneficiary->organizacion }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Anexo/Lugar: </b></strong>
                                                {{ $beneficiary->anexo }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>DNI: </b></strong>
                                                {{ $beneficiary->dni }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Nombres: </b></strong>
                                                {{ $beneficiary->nombres }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Apellidos: </b></strong>
                                                {{ $beneficiary->apellidos }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Fecha de nacimiento:</b></strong>
                                                {{ $beneficiary->fecha_nac }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Dirección: </b></strong>
                                                {{ $beneficiary->direccion }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Género:</b></strong>
                                                {{ $beneficiary->genero }}
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong><b>Ubigeo:</b></strong>
                                                {{ $beneficiary->ubigeo }}
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="d-flex justify-content-end items-center">
                                            <a href="{{ route('beneficiaries.index') }}"
                                               class="btn btn-success btn-round">
                                                <i class="ti-back-left"></i> Atrás</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

