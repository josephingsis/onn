@extends('layouts.app')
@section('content')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">

            <div class="main-body">
                <div class="page-wrapper">

                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Editar beneficiario</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('beneficiaries.index') }}">Beneficiarios</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Editar beneficiario</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">

                                    <div class="card-body">
                                        {!! Form::model($beneficiary, ['id'=>'beneficiario','method' => 'PATCH','route' => ['beneficiaries.update', $beneficiary->id]]) !!}
                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Iglesia/Organización:</strong>
                                                    {{ Form::select('organization_id', $organizations->pluck('nombre', 'id'), $beneficiary->organization_id, array('id' => 'organization_id','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Anexo/Lugar:</strong>
                                                    {{ Form::select('annexed_id', $annexeds->pluck('nombre', 'id'), $beneficiary->annexed_id, array('id' => 'annexed_id','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Periodo:</strong>
                                                    {{ Form::select('periodo', $periods->pluck('descripcion', 'id'), $beneficiary->period_id, array('id' => 'periodo','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>DNI:</strong>
                                                    {!! Form::text('dni', null, array('id'=>'dni','placeholder' => 'DNI','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Nombres:</strong>
                                                    {!! Form::text('nombres', null, array('id'=>'nombres','placeholder' => 'Nombres','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Apellidos:</strong>
                                                    {!! Form::text('apellidos', null, array('id'=>'apellidos','placeholder' => 'Apellidos','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Género:</strong>
                                                    <select name="genero" class="form-control show-tick" title="Seleccione...">
                                                        <option @if($beneficiary->genero === 'F') selected @endif>F</option>
                                                        <option @if($beneficiary->genero === 'M') selected @endif>M</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Fecha de nacimiento:</strong>
                                                    {!! Form::date('fecha_nac', null, array('id'=>'fecha_nac','placeholder' => 'Fecha de nacimiento','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Dirección:</strong>
                                                    {!! Form::text('direccion', null, array('id'=>'direccion','placeholder' => 'Dirección','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Departamento:</strong>
                                                    {{ Form::select('department_id',$departments->pluck('name', 'id'), (is_null($ubigeo) ? null : $ubigeo->department_id), array('id' => 'department_id','class' => 'selectpicker form-control','title'=>'Seleccione...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Provincia:</strong>
                                                    {{ Form::select('province_id',(is_null($ubigeo) ? [] : $provinces->pluck('name', 'id')) ,(is_null($ubigeo) ? null : $ubigeo->province_id), array('id' => 'province_id','class' => 'selectpicker form-control','title'=>'Seleccione...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Distrito:</strong>
                                                    {{ Form::select('ubigeo',(is_null($ubigeo) ? [] : $districts->pluck('name', 'id')), null, array('id' => 'ubigeo','class' => 'selectpicker form-control','title'=>'Seleccione...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary m-b-0">Guardar
                                                </button>
                                                <a href="{{route('beneficiaries.index')}}" type="submit"
                                                   class="btn btn-warning" type="reset">Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {

            cargar_anexos();

            function cargar_anexos() {
                $('#organization_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('organization.annexeds') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let anexos = '';
                            $.each(data, function (i, value) {
                                anexos = anexos + '<option value="' + value['id'] + '">' + value['nombre'] + '</option>';
                            });
                            $('#annexed_id').html(anexos)
                                .selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }
        });

    </script>
@endsection
