@extends('layouts.app')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">

            <div class="main-body">
                <div class="page-wrapper">

                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Agregar anexo</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('annexeds.index') }}">Anexo</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Agregar anexo</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        <form class="form-horizontal" id="anexo" method="POST"
                                              action="{{route('annexeds.store')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Denominación:</strong>
                                                        {{ Form::select('denomination_id', $denominations->pluck('nombre', 'id'), null, array('id' => 'denomination_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Iglesia/Organización:</strong>
                                                        {{ Form::select('organization_id', [], null, array('id' => 'organization_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Nombre de Anexo/Lugar:</strong>
                                                        {!! Form::text('nombre', null, array('id'=>'nombre','placeholder' => 'Nombre','class' => 'form-control','required')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Responsable:</strong>
                                                        {{ Form::select('responsible_id', $responsibles->pluck('nombre', 'id'), null, array('id' => 'responsible_id','class' => 'selectpicker','title'=>'Seleccione...','data-style'=>'form-control','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Teléfono:</strong>
                                                        {!! Form::text('telefono', null, array('id'=>'telefono','placeholder' => 'Teléfono','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-4 col-md-4">
                                                    <div class="form-group">
                                                        <strong>Dirección:</strong>
                                                        {!! Form::text('direccion', null, array('id'=>'direccion','placeholder' => 'Dirección','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" class="btn btn-primary m-b-0">Guardar
                                                    </button>
                                                    <a href="{{route('annexeds.index')}}" type="button"
                                                       class="btn btn-warning" type="reset">Cancelar</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            cargar_organizacion();

            function cargar_organizacion() {
                $('#denomination_id').on('change', function () {
                    let id = $(this).val();
                    console.log(id)
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('denomination.organizations') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let organizaciones = '';
                            $.each(data, function (i, value) {
                                organizaciones = organizaciones + '<option value="' + value['id'] + '">' + value['nombre'] + '</option>';
                            });
                            $('#organization_id').html(organizaciones)
                                .selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }
        });

    </script>
@endsection


