@extends('layouts.app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body breadcrumb-page">
                        <div class="card page-header p-0">
                            <div class="card-block front-icon-breadcrumb row align-items-end">
                                <div class="breadcrumb-header col">
                                    <div class="big-icon">
                                        <i class="feather icon-lock"></i>
                                    </div>
                                    <div class="d-inline-block">
                                        <h5>Roles</h5>
                                        <span>Listar</span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="pull-right">
                                        <a class="btn btn-success btn-round btn-sm"
                                           href="{{ route('roles.create') }}"><i
                                                class="feather icon-plus"></i> Nuevo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="card">

                                    <div class="card-header">

                                    </div>

                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="table"
                                                   class="table table-striped table-bordered nowrap">
                                                <thead>
                                                <tr>
                                                    <th>N°</th>
                                                    <th>Nombre</th>
                                                    <th width="280px">Acciones</th>
                                                </tr>
                                                @foreach ($roles as $key => $role)
                                                    <tr>
                                                        <th>{{ ++$i }}</th>
                                                        <th>{{ $role->name }}</th>
                                                        <td>
                                                            <a class="btn btn-info"
                                                               href="{{ route('roles.show',$role->id) }}">Ver</a>
                                                            @can('role-edit')
                                                                <a class="btn btn-success"
                                                                   href="{{ route('roles.edit',$role->id) }}">Editar</a>
                                                            @endcan
                                                            @can('role-delete')
                                                                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                                                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                                                {!! Form::close() !!}
                                                            @endcan
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! $roles->render() !!}
@endsection

