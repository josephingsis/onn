@extends('layouts.app')
@section('content')

    <div class="pcoded-content">
        <div class="pcoded-inner-content">

            <div class="main-body">
                <div class="page-wrapper">

                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Rol</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Ver rol</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <strong><b>Nombre:</b></strong>
                                                {{ $role->name }}
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <strong><b>Permisos:</b></strong>
                                                @if(!empty($rolePermissions))
                                                    @foreach($rolePermissions as $v)
                                                        <label class="label label-success">{{ $v->name }},</label>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="d-flex justify-content-end items-center">
                                            <a href="{{ route('roles.index') }}"
                                               class="btn btn-success btn-round btn-sm ">
                                                <i class="ti-back-left"></i> Atrás</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection