@extends('layouts.app')
@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Editar Distribución de cajas ONN</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('boxes.index') }}">Distribución de cajas ONN</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Editar distribución</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        {!! Form::model($box, ['id'=>'cajas','method' => 'PATCH','route' => ['boxes.update', $box->id]]) !!}
                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Iglesia/Organización:</strong>
                                                    {{ Form::select('organization_id', $organizations->pluck('nombre', 'id'), $box->organization_id, array('id' => 'organization_id','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Anexo/Lugar:</strong>
                                                    {{ Form::select('annexed_id', $annexeds->pluck('nombre', 'id'), $box->annexed_id, array('id' => 'annexed_id','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Periodo:</strong>
                                                    {{ Form::select('periodo', $periods->pluck('descripcion', 'id'), $box->period_id, array('id' => 'periodo','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required','multiple','data-max-options'=>'1')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Responsable:</strong>
                                                    {{ Form::select('responsible_id', $responsibles->pluck('nombre', 'id'), $box->responsible_id, array('id' => 'responsible_id','class' => 'selectpicker','title'=>'Seleccione ...','data-width'=>'100%','data-live-search' => 'true', 'required')) }}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>Contacto ONN:</strong>
                                                    {!! Form::text('contacto', null, array('id'=>'contacto','placeholder' => 'Nombres y Apellidos','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° Cajas:</strong>
                                                    {!! Form::number('cajas', null, array('id'=>'cajas','placeholder' => 'Cajas','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° EMR:</strong>
                                                    {!! Form::number('emr', null, array('id'=>'emr','placeholder' => 'N° de libros EMR','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° LGA:</strong>
                                                    {!! Form::number('lga', null, array('id'=>'lga','placeholder' => 'N° de libros LGA','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                <strong>N° NT:</strong>
                                                    {!! Form::number('nt', null, array('id'=>'nt','placeholder' => 'N° de Nuevo Testamento','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° GM:</strong>
                                                    {!! Form::number('gm', null, array('gm'=>'nt','placeholder' => 'N° de Guías del maestro','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° L8:</strong>
                                                    {!! Form::number('l8', null, array('id'=>'l8','placeholder' => 'N° de Libros Lección 8','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° CM:</strong>
                                                    {!! Form::number('cm', null, array('id'=>'cm','placeholder' => 'N° de Libros CM','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <strong>N° P:</strong>
                                                    {!! Form::number('p', null, array('id'=>'p','placeholder' => 'N° de Libros P','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary m-b-0">Guardar
                                                </button>
                                                <a href="{{route('boxes.index')}}" type="submit"
                                                   class="btn btn-warning" type="reset">Cancelar</a>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            cargar_anexos();

            function cargar_anexos() {
                $('#organization_id').on('change', function () {
                    let id = $(this).val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('organization.annexeds') }}",
                        data: {
                            "id": id
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (data) {
                            let anexos = '';
                            $.each(data, function (i, value) {
                                anexos = anexos + '<option value="' + value['id'] + '">' + value['nombre'] + '</option>';
                            });
                            $('#annexed_id').html(anexos)
                                .selectpicker('refresh');
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                });
            }
        });
    </script>
@endsection