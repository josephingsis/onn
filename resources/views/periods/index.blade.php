@extends('layouts.app')

@section('content')
    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body breadcrumb-page">
                        <div class="card page-header p-0">
                            <div class="card-block front-icon-breadcrumb row align-items-end">
                                <div class="breadcrumb-header col">
                                    <div class="big-icon">
                                        <i class="feather icon-calendar"></i>
                                    </div>
                                    <div class="d-inline-block">
                                        <h5>Periodos</h5>
                                        <span>Listar</span>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="pull-right">
                                        <a class="btn btn-success btn-round btn-sm"
                                           href="{{ route('periods.create') }}"><i
                                                class="feather icon-plus"></i> Nuevo</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table id="table"
                                       class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Descripción</th>
                                        <th width="280px">Acciones</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        let tabla = $('#table').DataTable(
            {
                "processing": true,
                "serverSide": true,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "No results matched": "No se encontraron resultados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    },
                },
                "ajax": {
                    "url": "{{ route('periods.all') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}"
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "descripcion"},
                    {"data": "options"}],
                "columnDefs": [
                    {"className": "text-center", "targets": [0, 1]},
                    {"bSortable": false, "aTargets": [2]},
                ],

            }
        );
        $('#table tbody').on('click', '.delete-confirm', function () {
            let id = $(this).attr('data-id');
            let url = '{!! url('periods') !!}/' + id;
            Swal.fire({
                title: '¿Estás seguro?',
                text: "¡No podrás revertir esto!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, elimínalo!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            '_method': 'DELETE',
                        },
                        dataType: 'JSON',
                        beforeSend: function () {
                        },
                        success: function (response) {
                            if (response.success) {
                                Swal.fire(
                                    '¡Correcto!',
                                    'El registro ha sido eliminado.',
                                    'success'
                                );
                                tabla.ajax.reload();
                            } else {
                                Swal.fire(
                                    '¡Incorrecto!',
                                    'Hubo un error al eliminar.',
                                    'error'
                                );
                            }
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            })
        });

    </script>
@endsection
