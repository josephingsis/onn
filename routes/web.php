<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    //$arr_ip = geoip()->getLocation('104.114.170.0'); // Chile
    //$arr_ip = geoip()->getLocation('104.73.192.0'); // Mexico
    $arr_ip = geoip()->getLocation(\Request::getClientIp(true)); // Mexico
    if($arr_ip->iso_code === 'MX'){
        return view('mexico.welcome');
    }else if($arr_ip->iso_code === 'CL'){
        return view('chile.welcome');
    }else{
        return view('welcome');
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('responsibles', 'ResponsibleController');
    Route::post('responsibles/all', 'ResponsibleController@responsibles_all')->name('responsibles.all');
    Route::resource('denominations', 'DenominationController');
    Route::post('denominations/all', 'DenominationController@denominations_all')->name('denominations.all');
    Route::resource('organizations', 'OrganizationController');
    Route::post('organizations/all', 'OrganizationController@organizations_all')->name('organizations.all');
    Route::resource('periods', 'PeriodController');
    Route::post('periods/all', 'PeriodController@periods_all')->name('periods.all');
    Route::resource('beneficiaries', 'BeneficiaryController');
    Route::post('beneficiaries/all', 'BeneficiaryController@beneficiaries_all')->name('beneficiaries.all');
    Route::resource('annexeds', 'AnnexedController');
    Route::post('annexeds/all', 'AnnexedController@annexeds_all')->name('annexeds.all');
    Route::resource('boxes', 'BoxController');
    Route::post('boxes/all', 'BoxController@boxes_all')->name('boxes.all');
    Route::post('department/provinces', 'UbigeoController@department_provinces')->name('department.provinces');
    Route::post('province/districts', 'UbigeoController@province_districts')->name('province.districts');
    Route::post('denomination/organizations', 'AnnexedController@denomination_organizations')->name('denomination.organizations');
    Route::post('organization/annexeds', 'BeneficiaryController@organization_annexeds')->name('organization.annexeds');
    Route::post('organization/annexeds', 'BoxController@organization_annexeds')->name('organization.annexeds');

});
