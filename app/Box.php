<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $fillable=[
        'contacto','cajas','emr','lga','nt','gm','l8','cm','p','responsible_id','organization_id','annexed_id', 'period_id'
    ];
}
