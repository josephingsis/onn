<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable=[
        'nombre','direccion','ubigeo','estado','responsible_id', 'denomination_id'
    ];
}
