<?php

namespace App\Http\Controllers;


use App\Annexed;
use App\Client;
use App\Denomination;
use App\Organization;
use App\Responsible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:organization-list');
        $this->middleware('permission:organization-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:organization-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:organization-delete', ['only' => ['destroy']]);
    }


    public function index()
    {
        return view('organizations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = DB::table('departments')->get();
        $denominations = Denomination::all();
        $responsibles = Responsible::all();
        return view('organizations.create', ['denominations' => $denominations,'responsibles' => $responsibles, 'departments' => $departments]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre' => 'required|unique:organizations',
        ]);
        $input = $request->all();
        Organization::create($input);
        return redirect()->route('organizations.index')->with('success', 'Organización registrado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organization = DB::table('organizations')
            ->join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->join('responsibles', 'organizations.responsible_id', 'responsibles.id')
            ->select('organizations.*', 'denominations.nombre as denominacion','responsibles.nombre as responsable')
            ->where('organizations.id', '=', $id)
            ->first();
        return view('organizations.show', ['organization' => $organization]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = DB::table('departments')->get();
        $denominations = Denomination::all();
        $responsibles = Responsible::all();
        $organization = DB::table('organizations')
            ->join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->join('responsibles', 'organizations.responsible_id', 'responsibles.id')
            ->select('organizations.*', 'denominations.nombre as denominacion','responsibles.nombre as responsable')
            ->where('organizations.id', '=', $id)
            ->first();
        if (!is_null($organization->ubigeo)) {
            $ubigeo = DB::table('districts')->where('id', '=', $organization->ubigeo)->first();
            $provinces = DB::table('provinces')->where('department_id', '=', $ubigeo->department_id)->get();
            $districts = DB::table('districts')->where('province_id', '=', $ubigeo->province_id)->get();
        } else {
            $ubigeo = null;
            $provinces = [];
            $districts = [];
        }
        return view('organizations.edit', ['organization' => $organization,'responsibles' => $responsibles, 'denominations'=>$denominations,'departments' => $departments, 'provinces' => $provinces, 'districts' => $districts, 'ubigeo' => $ubigeo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        $input = $request->all();
        $organization->update($input);
        return redirect()->route('organizations.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organization = Organization::find($id);
        $organization->delete();
        return response()->json(['success' => true]);
    }

    public function organizations_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'responsable',
            2 => 'nombre',
            3 => 'direccion',
            4 => 'ubigeo',
            5 => 'denominacion',
        );

        $totalData = Organization::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $organizations = DB::table('organizations')
                ->join('denominations', 'organizations.denomination_id', 'denominations.id')
                ->join('responsibles', 'organizations.responsible_id', 'responsibles.id')
                ->select('organizations.*', 'denominations.nombre as denominacion','responsibles.nombre as responsable')
                ->where('organizations.nombre', 'LIKE', "%{$search}%");

        } else {
            $search = $request->input('search.value');
            $organizations = DB::table('organizations')
                ->join('denominations', 'organizations.denomination_id', 'denominations.id')
                ->join('responsibles', 'organizations.responsible_id', 'responsibles.id')
                ->select('organizations.*', 'denominations.nombre as denominacion','responsibles.nombre as responsable')
                ->where('organizations.nombre', 'LIKE', "%{$search}%");

        }
        $totalFiltered = $organizations->count();
        $organizations = $organizations->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($organizations)) {
            foreach ($organizations as $i => $organization) {
                $show = route('organizations.show', $organization->id);
                $edit = route('organizations.edit', $organization->id);
                $destroy = route('organizations.destroy', $organization->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('organization-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('organization-delete')) {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$organization->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['nombre'] = $organization->nombre;
                $nestedData['responsable'] = $organization->responsable;
                $nestedData['direccion'] = $organization->direccion;
                $nestedData['ubigeo'] = $organization->ubigeo;
                $nestedData['denominacion'] = $organization->denominacion;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }
}
