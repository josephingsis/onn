<?php

namespace App\Http\Controllers;

use App\Annexed;
use App\Box;
use App\Organization;
use App\Period;
use App\Responsible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function organization_annexeds(Request $request)
    {
        $annexeds = DB::table('annexeds')->where('organization_id', '=', $request->id)
            ->get();
        echo json_encode($annexeds);
    }

    public function index()
    {
        return view('boxes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responsibles = Responsible::all();
        $organizations = Organization::join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->select('organizations.id', DB::raw("CONCAT(denominations.nombre,' - ',organizations.nombre) AS nombre"));
        $annexeds = Annexed::all();
        $periods = Period::all();
        return view('boxes.create', ['responsibles' => $responsibles,'organizations' => $organizations,'annexeds' => $annexeds, 'periods' => $periods]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'cajas' => 'required',
        ]);
        $input = $request->all();
        Box::create($input);
        return redirect()->route('boxes.index')->with('success', 'Distribución de cajas registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $box = DB::table('boxes')
            ->join('responsibles', 'boxes.responsible_id', 'responsibles.id')
            ->join('organizations', 'boxes.organization_id', 'organizations.id')
            ->join('annexeds', 'boxes.annexed_id', 'annexeds.id')
            ->join('periods', 'boxes.period_id', 'periods.id')
            ->select('boxes.*','responsibles.nombre as responsable','organizations.nombre as organizacion','annexeds.nombre as anexo', 'periods.descripcion as periodo')
            ->where('boxes.id', '=', $id)
        ->first();
        return view('boxes.show', ['box' => $box]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responsibles = Responsible::all();
        $periods = Period::all();
        $organizations = Organization::join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->select('organizations.id', DB::raw("CONCAT(denominations.nombre,' - ',organizations.nombre) AS nombre"))->get();
        $box = DB::table('boxes')
            ->join('responsibles', 'boxes.responsible_id', 'responsibles.id')
            ->join('organizations', 'boxes.organization_id', 'organizations.id')
            ->join('annexeds', 'boxes.annexed_id', 'annexeds.id')
            ->join('periods', 'boxes.period_id', 'periods.id')
            ->select('boxes.*','responsibles.nombre as responsable','organizations.nombre as organizacion','annexeds.nombre as anexo', 'periods.descripcion as periodo')
            ->where('boxes.id', '=', $id)
            ->first();
        $annexeds = Annexed::where('annexeds.id',$box>'annexed_id')->get();

        return view('boxes.edit', ['box' => $box, 'responsibles' => $responsibles,'periods' => $periods,'organizations'=>$organizations,'annexeds' => $annexeds]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Box $box)
    {
        $input = $request->all();
        $box->update($input);
        return redirect()->route('boxes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $box = Box::find($id);
        $box->delete();
        return response()->json(['success' => true]);
    }

    public function boxes_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'contacto',
            2 => 'cajas',
            3 => 'emr',
            4 => 'lga',
            5 => 'nt',
            6 => 'gm',
            7 => 'l8',
            8 => 'cm',
            9 => 'p',
            10 => 'organizacion',
            11 => 'anexo',
            12 => 'responsable',
            13 => 'periodo',

        );

        $totalData = Box::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $boxes = DB::table('boxes')
                ->join('responsibles', 'boxes.responsible_id', 'responsibles.id')
                ->join('organizations', 'boxes.organization_id', 'organizations.id')
                ->join('annexeds', 'boxes.annexed_id', 'annexeds.id')
                ->join('periods', 'boxes.period_id', 'periods.id')
                ->select('boxes.*','responsibles.nombre as responsable','organizations.nombre as organizacion','annexeds.nombre as anexo', 'periods.descripcion as periodo')
                ->where('boxes.cajas', 'LIKE', "%{$search}%");
        } else {
            $search = $request->input('search.value');
            $boxes = DB::table('boxes')
                ->join('responsibles', 'boxes.responsible_id', 'responsibles.id')
                ->join('organizations', 'boxes.organization_id', 'organizations.id')
                ->join('annexeds', 'boxes.annexed_id', 'annexeds.id')
                ->join('periods', 'boxes.period_id', 'periods.id')
                ->select('boxes.*','responsibles.nombre as responsable','organizations.nombre as organizacion','annexeds.nombre as anexo', 'periods.descripcion as periodo')
                ->where('boxes.cajas', 'LIKE', "%{$search}%");

        }
        $totalFiltered = $boxes->count();
        $boxes = $boxes->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($boxes)) {
            foreach ($boxes as $i => $box) {
                $show = route('boxes.show', $box->id);
                $edit = route('boxes.edit', $box->id);
                $destroy = route('boxes.destroy', $box->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('box-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('box-delete'))
                {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$box->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['contacto'] = $box->contacto;
                $nestedData['cajas'] = $box->cajas;
                $nestedData['emr'] = $box->emr;
                $nestedData['lga'] = $box->lga;
                $nestedData['nt'] = $box->nt;
                $nestedData['gm'] = $box->gm;
                $nestedData['l8'] = $box->l8;
                $nestedData['cm'] = $box->cm;
                $nestedData['p'] = $box->p;
                $nestedData['organizacion'] = $box->organizacion;
                $nestedData['anexo'] = $box->anexo;
                $nestedData['responsable'] = $box->responsable;
                $nestedData['periodo'] = $box->periodo;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

}
