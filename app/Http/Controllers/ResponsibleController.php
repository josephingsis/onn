<?php

namespace App\Http\Controllers;

use App\Responsible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ResponsibleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:responsible-list');
        $this->middleware('permission:responsible-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:responsible-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:responsible-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('responsibles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('responsibles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre' => 'required|unique:denominations',
        ]);
        $responsibles = $request->all();
        Responsible::create($responsibles);
        return redirect()->route('responsibles.index')->with('success', 'Responsable registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $responsible = Responsible::find($id);
        return view('responsibles.show', compact('responsible'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responsible = Responsible::find($id);
        return view('responsibles.edit', ['responsible' => $responsible]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Responsible $responsible)
    {
        request()->validate([
            'nombre' => 'required|unique:responsibles,nombre,'.$responsible->id,
        ]);
        $input = $request->all();
        $responsible->update($input);
        return redirect()->route('responsibles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $responsible = Responsible::find($id);
        $responsible->delete();
        return response()->json(['success' => true]);
    }

    public function responsibles_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'nombre',
        );

        $totalData = Responsible::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $responsibles = DB::table('responsibles');
        } else {
            $search = $request->input('search.value');

            $responsibles = DB::table('responsibles')
                ->where('$responsibles.nombre', 'LIKE', "%{$search}%");
        }
        $totalFiltered = $responsibles->count();
        $responsibles = $responsibles->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($responsibles)) {
            foreach ($responsibles as $i => $responsible) {
                $show = route('responsibles.show', $responsible->id);
                $edit = route('responsibles.edit', $responsible->id);
                $destroy = route('responsibles.destroy', $responsible->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('responsible-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('responsible-delete')) {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$responsible->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['nombre'] = $responsible->nombre;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }
}
