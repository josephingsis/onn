<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UbigeoController extends Controller
{
    public function department_provinces(Request $request)
    {
        $provinces = DB::table('provinces')->where('department_id', '=', $request->id)
            ->get();
        echo json_encode($provinces);
    }

    public function province_districts(Request $request)
    {
        $districts = DB::table('districts')->where('province_id', '=', $request->id)
            ->get();
        echo json_encode($districts);
    }

}
