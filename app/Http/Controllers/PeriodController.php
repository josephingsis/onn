<?php

namespace App\Http\Controllers;

use App\Period;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:period-list');
        $this->middleware('permission:period-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:period-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:period-delete', ['only' => ['destroy']]);
    }


    public function index()
    {
        return view('periods.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('periods.create');
    }

    /**
     * Registra un nuevo periodo
     *
     * @param Request $request
     * @return Redirect
     */
    public function store(Request $request)
    {
        request()->validate([
            'descripcion' => 'required|unique:periods',
        ]);
        $periods = $request->all();
        Period::create($periods);
        return redirect()->route('periods.index')->with('success', 'Periodo registrado correctamente');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $period = Period::find($id);
        return view('periods.show', compact('period'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $period = Period::find($id);
        return view('periods.edit', ['period' => $period]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Period $period
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Period $period)
    {
        request()->validate([
            'descripcion' => 'required|unique:periods,descripcion,'.$period->id,
        ]);
        $input = $request->all();
        $period->update($input);
        return redirect()->route('periods.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $period = Period::find($id);
        $period->delete();
        return response()->json(['success' => true]);
    }

    public function periods_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'descripcion',

        );

        $totalData = Period::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $periods = DB::table('periods');
        } else {
            $search = $request->input('search.value');

            $periods = DB::table('periods')
                ->where('periods.descripcion', 'LIKE', "%{$search}%");
        }
        $totalFiltered = $periods->count();
        $periods = $periods->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($periods)) {
            foreach ($periods as $i => $period) {
                $show = route('periods.show', $period->id);
                $edit = route('periods.edit', $period->id);
                $destroy = route('periods.destroy', $period->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('period-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('period-delete')) {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$period->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['descripcion'] = $period->descripcion;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }
}
