<?php

namespace App\Http\Controllers;

use App\Denomination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DenominationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:denomination-list');
        $this->middleware('permission:denomination-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:denomination-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:denomination-delete', ['only' => ['destroy']]);
    }


    public function index()
    {
        return view('denominations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('denominations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre' => 'required|unique:denominations',
        ]);
        $denominations = $request->all();
        Denomination::create($denominations);
        return redirect()->route('denominations.index')->with('success', 'Denominación registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $denomination = Denomination::find($id);
        return view('denominations.show', compact('denomination'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $denomination = Denomination::find($id);
        return view('denominations.edit', ['denomination' => $denomination]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $denomination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, denomination $denomination)
    {
        request()->validate([
            'nombre' => 'required|unique:denominations,nombre,'.$denomination->id,
        ]);
        $input = $request->all();
        $denomination->update($input);
        return redirect()->route('denominations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $denomination = Denomination::find($id);
        $denomination->delete();
        return response()->json(['success' => true]);
    }

    public function denominations_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'nombre',
        );

        $totalData = Denomination::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $denominations = DB::table('denominations');
        } else {
            $search = $request->input('search.value');

            $denominations = DB::table('denominations')
                ->where('denominations.nombre', 'LIKE', "%{$search}%");
        }
        $totalFiltered = $denominations->count();
        $denominations = $denominations->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($denominations)) {
            foreach ($denominations as $i => $denomination) {
                $show = route('denominations.show', $denomination->id);
                $edit = route('denominations.edit', $denomination->id);
                $destroy = route('denominations.destroy', $denomination->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('denomination-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('denomination-delete')) {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$denomination->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['nombre'] = $denomination->nombre;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

}
