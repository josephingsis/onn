<?php

namespace App\Http\Controllers;

use App\Annexed;
use App\Denomination;
use App\Organization;
use App\Responsible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AnnexedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function denomination_organizations(Request $request)
    {
        $organizations = DB::table('organizations')->where('denomination_id', '=', $request->id)
            ->get();
        echo json_encode($organizations);
    }


    public function index()
    {
        return view('annexeds.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $denominations = Denomination::all();
        $responsibles = Responsible::all();
        return view('annexeds.create', ['denominations' => $denominations,'responsibles' => $responsibles]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre' => 'required|unique:annexeds',
        ]);
        $input = $request->all();
        Annexed::create($input);
        return redirect()->route('annexeds.index')->with('success', 'Anexo registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $annexed = DB::table('annexeds')
            ->join('responsibles', 'annexeds.responsible_id', 'responsibles.id')
            ->join('organizations', 'annexeds.organization_id', 'organizations.id')
            ->join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->select('annexeds.*','responsibles.nombre as responsable','organizations.nombre as organizacion','denominations.nombre as denominacion')
            ->where('annexeds.id', '=', $id)
            ->first();
        return view('annexeds.show', ['annexed' => $annexed]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $responsibles = Responsible::all();
        $denominations = Denomination::all();
        $annexed = DB::table('annexeds')
            ->join('responsibles', 'annexeds.responsible_id', 'responsibles.id')
            ->join('organizations', 'annexeds.organization_id', 'organizations.id')
            ->join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->select('annexeds.*','responsibles.nombre as responsable','organizations.nombre as organizacion','denominations.nombre as denominacion','denominations.id as denomination_id')
            ->where('annexeds.id', '=', $id)
            ->first();
        $organizations = Organization::where('denomination_id',$annexed->denomination_id)->get();

        return view('annexeds.edit', ['annexed' => $annexed, 'responsibles'=>$responsibles,'organizations'=>$organizations,'denominations'=>$denominations ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Annexed  $annexed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Annexed $annexed)
    {
        $input = $request->all();
        $annexed->update($input);
        return redirect()->route('annexeds.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $annexed = Annexed::find($id);
        $annexed->delete();
        return response()->json(['success' => true]);
    }

    public function annexeds_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'nombre',
            2 => 'telefono',
            3 => 'direccion',
            4 => 'responsable',
            5 => 'organizacion',
            6 => 'denominacion',

        );

        $totalData = Annexed::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $annexeds = DB::table('annexeds')
                ->join('responsibles', 'annexeds.responsible_id', 'responsibles.id')
                ->join('organizations', 'annexeds.organization_id', 'organizations.id')
                ->join('denominations', 'organizations.denomination_id', 'denominations.id')
                ->select('annexeds.*','responsibles.nombre as responsable','organizations.nombre as organizacion','denominations.nombre as denominacion')
                ->where('annexeds.nombre', 'LIKE', "%{$search}%");
        } else {
            $search = $request->input('search.value');
            $annexeds = DB::table('annexeds')
                ->join('responsibles', 'annexeds.responsible_id', 'responsibles.id')
                ->join('organizations', 'annexeds.organization_id', 'organizations.id')
                ->join('denominations', 'organizations.denomination_id', 'denominations.id')
                ->select('annexeds.*','responsibles.nombre as responsable','organizations.nombre as organizacion','denominations.nombre as denominacion','denominations.nombre as denominacion')
                ->where('annexeds.organizacion', 'LIKE', "%{$search}%");

        }
        $totalFiltered = $annexeds->count();
        $annexeds = $annexeds->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($annexeds)) {
            foreach ($annexeds as $i => $annexed) {
                $show = route('annexeds.show', $annexed->id);
                $edit = route('annexeds.edit', $annexed->id);
                $destroy = route('annexeds.destroy', $annexed->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('annexed-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('annexed-delete')) {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$annexed->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['nombre'] = $annexed->nombre;
                $nestedData['telefono'] = $annexed->telefono;
                $nestedData['direccion'] = $annexed->direccion;
                $nestedData['responsable'] = $annexed->responsable;
                $nestedData['organizacion'] = $annexed->organizacion;
                $nestedData['denominacion'] = $annexed->denominacion;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }


}
