<?php

namespace App\Http\Controllers;

use App\Annexed;
use App\Beneficiary;
use App\Organization;
use App\Period;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BeneficiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function organization_annexeds(Request $request)
    {
        $annexeds = DB::table('annexeds')->where('organization_id', '=', $request->id)
            ->get();
        echo json_encode($annexeds);
    }

    public function index()
    {
        return view('beneficiaries.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = DB::table('departments')->get();
        $organizations = Organization::join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->select('organizations.id', DB::raw("CONCAT(denominations.nombre,' - ',organizations.nombre) AS nombre"));
        $annexeds = Annexed::all();
        $periods = Period::all();
        return view('beneficiaries.create', ['organizations' => $organizations, 'periods' => $periods, 'annexeds' => $annexeds, 'departments' => $departments]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'dni' => 'required|unique:beneficiaries',
        ]);
        $input = $request->all();
        Beneficiary::create($input);
        return redirect()->route('beneficiaries.index')->with('success', 'Beneficiario registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $beneficiary = DB::table('beneficiaries')
            ->join('organizations', 'beneficiaries.organization_id', 'organizations.id')
            ->join('annexeds', 'beneficiaries.annexed_id', 'annexeds.id')
            ->join('periods', 'beneficiaries.period_id', 'periods.id')
            ->select('beneficiaries.*', 'organizations.nombre as organizacion', 'periods.descripcion as periodo', 'annexeds.nombre as anexo')
            ->where('beneficiaries.id', '=', $id)
            ->first();
        return view('beneficiaries.show', ['beneficiary' => $beneficiary]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = DB::table('departments')->get();
        $periods = Period::all();
        $organizations = Organization::join('denominations', 'organizations.denomination_id', 'denominations.id')
            ->select('organizations.id', DB::raw("CONCAT(denominations.nombre,' - ',organizations.nombre) AS nombre"))->get();

        $beneficiary = DB::table('beneficiaries')
            ->join('organizations', 'beneficiaries.organization_id', 'organizations.id')
            ->join('annexeds', 'beneficiaries.annexed_id', 'annexeds.id')
            ->join('periods', 'beneficiaries.period_id', 'periods.id')
            ->select('beneficiaries.*', 'periods.id as periodo', 'organizations.id as organizacion', 'annexeds.id as anexo')
            ->where('beneficiaries.id', '=', $id)
            ->first();
        $annexeds = Annexed::where('annexeds.id',$beneficiary->annexed_id)->get();

        if (!is_null($beneficiary->ubigeo)) {
            $ubigeo = DB::table('districts')->where('id', '=', $beneficiary->ubigeo)->first();
            $provinces = DB::table('provinces')->where('department_id', '=', $ubigeo->department_id)->get();
            $districts = DB::table('districts')->where('province_id', '=', $ubigeo->province_id)->get();
        } else {
            $ubigeo = null;
            $provinces = [];
            $districts = [];
        }
        return view('beneficiaries.edit', ['beneficiary' => $beneficiary, 'periods' => $periods, 'organizations' => $organizations,'annexeds' => $annexeds,'departments' => $departments, 'provinces' => $provinces, 'districts' => $districts, 'ubigeo' => $ubigeo]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Beneficiary $beneficiary)
    {
        $input = $request->all();
        $beneficiary->update($input);
        return redirect()->route('beneficiaries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beneficiary = Beneficiary::find($id);
        $beneficiary->delete();
        return response()->json(['success' => true]);
    }

    public function beneficiaries_all(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'dni',
            2 => 'nombres',
            3 => 'apellidos',
            4 => 'direccion',
            5 => 'genero',
            6 => 'nacimiento',
            7 => 'ubigeo',
            8 => 'organizacion',
            9 => 'anexo',
            10 => 'periodo',

        );

        $totalData = Beneficiary::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $beneficiaries = DB::table('beneficiaries')
                ->join('organizations', 'beneficiaries.organization_id', 'organizations.id')
                ->join('periods', 'beneficiaries.period_id', 'periods.id')
                ->join('annexeds', 'beneficiaries.annexed_id', 'annexeds.id')
                ->select('beneficiaries.*', 'organizations.nombre as organizacion', 'periods.descripcion as periodo', 'annexeds.nombre as anexo')
                ->where('beneficiaries.nombres', 'LIKE', "%{$search}%");
        } else {
            $search = $request->input('search.value');
            $beneficiaries = DB::table('beneficiaries')
                ->join('organizations', 'beneficiaries.organization_id', 'organizations.id')
                ->join('periods', 'beneficiaries.period_id', 'periods.id')
                ->join('annexeds', 'beneficiaries.annexed_id', 'annexeds.id')
                ->select('beneficiaries.*', 'organizations.nombre as organizacion', 'periods.descripcion as periodo', 'annexeds.nombre as anexo')
                ->where('beneficiaries.nombres', 'LIKE', "%{$search}%");

        }
        $totalFiltered = $beneficiaries->count();
        $beneficiaries = $beneficiaries->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
        $data = array();
        if (!empty($beneficiaries)) {
            foreach ($beneficiaries as $i => $beneficiary) {
                $show = route('beneficiaries.show', $beneficiary->id);
                $edit = route('beneficiaries.edit', $beneficiary->id);
                $destroy = route('beneficiaries.destroy', $beneficiary->id);
                $buttons = "<input type='hidden' name='_token' id='csrf-token' value='" . Session::token() . "' />
<a class='btn btn-info btn-sm' href='{$show}'><i
                            class='demo-pli-file-search'></i> Ver</a> ";

                if (Auth::user()->hasPermissionTo('beneficiary-edit')) {
                    $buttons = $buttons . "<a class='btn btn-success btn-sm'
                       href='{$edit}'><i class='demo-pli-pencil'></i> Editar</a> ";
                }
                if (Auth::user()->hasPermissionTo('beneficiary-delete')) {
                    $buttons = $buttons . "<button type='button' class='btn btn-danger btn-sm delete-confirm' data-id='{$beneficiary->id}'><i class='demo-pli-trash'></i> Eliminar</button>";
                }

                $roles = '';
                if (!empty(Auth::user()->getRoleNames())) {
                    foreach (Auth::user()->getRoleNames() as $v) {
                        $roles = $roles . '<label class="badge badge-success">' . $v . '</label>';
                    }
                }

                $nestedData['id'] = ($i + 1);
                $nestedData['dni'] = $beneficiary->dni;
                $nestedData['nombres'] = $beneficiary->nombres . ' ' . $beneficiary->apellidos;
                $nestedData['direccion'] = $beneficiary->direccion;
                $nestedData['genero'] = $beneficiary->genero;
                $nestedData['nacimiento'] = $beneficiary->fecha_nac;
                $nestedData['ubigeo'] = $beneficiary->ubigeo;
                $nestedData['organizacion'] = $beneficiary->organizacion;
                $nestedData['anexo'] = $beneficiary->anexo;
                $nestedData['periodo'] = $beneficiary->periodo;
                $nestedData['options'] = "<div><form action='$destroy' method='POST'>" . $buttons . "</form></div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }


}
