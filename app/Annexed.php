<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annexed extends Model
{
    protected $fillable=[
        'nombre','telefono','direccion','responsible_id','organization_id'];
}
