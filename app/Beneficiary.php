<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    protected $fillable=[
        'dni','nombres','apellidos','direccion','ubigeo','genero','fecha_nac','organization_id',  'annexed_id','period_id'
    ];
}
